<?php
$lang['action_freshen'] = 'Opfrisk / Reparér en CMSMS %s installation';
$lang['action_install'] = 'Opretter en ny CMSMS-baseret hjemmeside';
$lang['action_upgrade'] = 'Opgraderer en CMSMS-hjemmeside til version %s';
$lang['advanced_mode'] = 'Anvend avanceret metode';
$lang['apptitle'] = 'Hjælper til installation og opgradering';
$lang['assets_dir_exists'] = 'Mappe til aktiver eksisterer';
$lang['available_languages'] = 'Tilgængelige sprog';
$lang['build_date'] = 'Kompileringsdato';
$lang['changelog_uc'] = 'Ændringslog';
$lang['cleaning_files'] = 'Fjerner filer, som ikke længere skal bruges af denne udgivelse';
$lang['config_writable'] = 'Tester om der kan skrives til konfiguationsfilen';
$lang['confirm_freshen'] = 'Er du sikker på, du vil opfriske (reparere) den eksisterende CMSMS-installation? Vær meget forsigtig!';
$lang['confirm_upgrade'] = 'Er du sikker på, du vil påbegynde opgraderingsprocessen?';
$lang['curl_extension'] = 'Tester Curl-udvidelse';
$lang['create_assets_structure'] = 'Opretter lokation til filressourcer';
$lang['database_support'] = 'Tjekker at der findes kompatible databasedrivere';
$lang['desc_wizard_step1'] = 'Begynd installations -eller opgraderingsproces';
$lang['desc_wizard_step2'] = 'Analyserer destinationsmappe mht. eksisterende software';
$lang['desc_wizard_step3'] = 'Tester og sikrer at alt er i orden således at CMSMS\' kerne kan installeres';
$lang['desc_wizard_step4'] = 'Indtast grundliggende konfiguraitons-info mhp. nye installationer og genopfriskende operationer';
$lang['desc_wizard_step5'] = 'Er der tale om en ny installation, så indtast info vedrørende administratorens konto';
$lang['desc_wizard_step6'] = 'Er der tale om en ny installation, så indtast nogle grundliggende detaljer om hjemmesiden';
$lang['desc_wizard_step7'] = 'Udpak filer';
$lang['desc_wizard_step8'] = 'Opret eller opdatér database-skema, sæt indledende handlinger, tilladelser, brugerkonti, skabeloner, typografiark og indhold';
$lang['desc_wizard_step9'] = 'Installér og/eller opgradér moduler, hvor det er nødvendigt, skriv konfigurationsfilen og ryd op.';
$lang['destination_directory'] = 'Destinationsmappe';
$lang['dest_writable'] = 'Opret tilladelser for destinationsmappen';
$lang['disable_functions'] = 'Tester for deaktiverede funktioner';
$lang['done'] = 'udført';
$lang['email_accountinfo_message'] = 'Installationen af CMS Made Simple er færdig.

Denne email indeholder følsomme informationer og bør gemmes et sikkert sted.

Her følger diverse oplysninger angående din installion.
Brugernavn: %s
Adgangskode: %s
Installationsmappe: %s
Rodadresse: %s';
$lang['email_accountinfo_message_exp'] = 'Installationen af CMS Made Simple er færdig.

Denne email indeholder følsomme informationer og bør gemmes et sikkert sted.

Her følger diverse oplysninger angående din installion.
Brugernavn: %s
Adgangskode: %s
Installationsmappe: %s';
$lang['email_accountinfo_subject'] = 'Installation af CMS Made Simple udført';
$lang['emailaccountinfo'] = 'Send email med kontooplysninger';
$lang['emailaddr'] = 'Email adresse';
$lang['error_adminacct_emailaddr'] = 'Den indtastede email adresse er ikke gyldig';
$lang['error_adminacct_emailaddrrequired'] = 'Du har angivet, at kontooplysningerne skal sendes pr., email, men den indtastede email adresse er ikke gyldig';
$lang['error_adminacct_password'] = 'Den indtastede adgangskode er ikke gyldig (den skal bestå af mindst seks tegn)';
$lang['error_adminacct_repeatpw'] = 'De to indtastede adgangskoder stemmer ikke overens';
$lang['error_adminacct_username'] = 'Det indtastede brugernavn er ikke gyldigt. Prøv venligst igen';
$lang['error_admindirrenamed'] = 'Det ser ud til at du, af sikkerhedsmæssige hensyn, har omdøbt din CMSMS administratormappe. Du er derfor nødt til at omgøre <a href="http://docs.cmsmadesimple.org/general-information/securing-cmsms#renaming-admin-folder" target="_blank" class="external">denne proces</a>, før du kan fortsætte!<br /><br />Når du har genoprettet oprindeligt navn på og sti til administrationsmappen, bedes du venligst genindlæse denne side.';
$lang['error_backupconfig'] = 'Vi var ikke i stand til at tage en ordentlig backup af konfigurationsfilen';
$lang['error_checksum'] = 'Tjeksummen for den udpakkede fil svarer ikke til den oprindelige';
$lang['error_cmstablesexist'] = 'Det ser ud til, at der allerede findes en installation af CMS i denne database. Indtast venligst informationer vedrørende en anden database. Hvis du godt vil bruge et andet præfiks til tabellerne, kan det være nødvendigt at genstarte installationsprocessen og så anvende den avancerede metode.';
$lang['error_createtable'] = 'Oprettelse af databasetabel stødte ind i problemer... Måske mangler de fornødne tilladelser';
$lang['error_dbconnect'] = 'Vi kunne ikke oprette forbindelse til databasen. Dobbelttjek venligst de oplysninger, som du har angivet';
$lang['error_dirnotvalid'] = 'Mappen %s findes ikke (eller der mangler en skrivetilladelse den)';
$lang['error_droptable'] = 'Sletning af databasetabel stødte ind i problemer... Måske mangler de fornødne tilladelser';
$lang['error_filenotwritable'] = 'Filen %s kunne ikke overskrives (problem med tilladelser)';
$lang['error_internal'] = 'Beklager, et eller andet er gået galt... (intern fejl) (%s)';
$lang['error_invalid_directory'] = 'Det ser ud til, at den mappe, som du har valgt til installationen, er den samme mappe som installalationsfilerne selv bruger';
$lang['error_invalidconfig'] = 'Der er enten en fejl i konfigurationsfilen eller denne fil mangler';
$lang['error_invaliddbpassword'] = 'Adgangskoden til databasen indeholder ugyldige tegn, som det ikke er forsvarligt at gemme';
$lang['error_invalidkey'] = 'Ugyldig medlemsvariabel eller nøgle %s for klassen %s';
$lang['error_invalidparam'] = 'Ugyldig parameter eller værdi for parametren %s';
$lang['error_invalidtimezone'] = 'Den angivne tidszone er ugyldig';
$lang['error_invalidqueryvar'] = 'Den indtastede variabel i forbindelse med forespørgslen indeholder ugyldige tegn. Anvend venligst kun alfanumeriske tegn samt underscore.';
$lang['error_missingconfigvar'] = 'Enten mangler nøglen "%s" eller denne er ugyldig i config.ini filen';
$lang['error_noarchive'] = 'Der er problemer med at finde den pakkede arkivfil... Genstart venligst';
$lang['error_nlsnotfound'] = 'Der er problemer med at finde NLS-filer i arkivfilen';
$lang['error_nodatabases'] = 'Der blev ikke fundet en kompatibel database-ekstension';
$lang['error_nodbhost'] = 'Indtast venligst et gyldigt værtsnavn (eller IP adresse) til databaseforbindelsen';
$lang['error_nodbname'] = 'Indtast venligst navnet på en gyldig database eller vært som specificeret ovenfor';
$lang['error_nodbpass'] = 'Indtast venligst en gyldig adgangskode som godkendelse til databasen';
$lang['error_nodbprefix'] = 'Indtast venligst et gyldigt præfiks til databasens tabeller';
$lang['error_nodbtype'] = 'Vælg venligst en databasetype';
$lang['error_nodbuser'] = 'Indtast venligst et gyldigt brugernavn som godkendelse til databasen';
$lang['error_nodestdir'] = 'Destinationsmappe er ikke angivet';
$lang['error_nositename'] = 'Navnet på hjemmesiden er et obligatorisk parameter. Indtast venligst et passende navn for din hjemmeside.';
$lang['error_notimezone'] = 'Angiv venligst en gyldig tidszone for denne server';
$lang['error_overwrite'] = 'Der er et problem vedrørende tilladelser: %s kan ikke overskrives';
$lang['error_sendingmail'] = 'Fejl under afsendelse af email';
$lang['error_tzlist'] = 'Der er problemer med at finde listen over tidszone-identifikatorer';
$lang['errorlevel_estrict'] = 'Tjekker E_STRICT';
$lang['errorlevel_edeprecated'] = 'Tjekker E_DEPRECATED';
$lang['edeprecated_enabled'] = 'E_DEPRECATED er i anvendelse i PHPs fejlrapporteringssektion. Selvom dette ikke forhindrer CMSMS i at fungere, så kan det resultere i, at der vises advarselsmeddelelser på skærmen - især fra ældre 3. parts moduler';
$lang['estrict_enabled'] = 'E_STRICT er i anvendelse i PHPs fejlrapporteringssektion. Selvom dette ikke forhindrer CMSMS i at fungere, så kan det resultere i, at der vises advarselsmeddelelser på skærmen - især fra ældre 3. parts moduler';
?>