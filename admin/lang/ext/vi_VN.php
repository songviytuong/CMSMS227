<?php
$lang['added_content']='Đ&atilde; th&ecirc;m Nội dung';
$lang['added_group']='Đ&atilde; th&ecirc;m Nh&oacute;m';
$lang['aliasalreadyused']='The supplied &quot;Page Alias&quot; is already in use on another page.  Change the &quot;Page Alias&quot; to something else.';
$lang['always']='Lu&ocirc;n lu&ocirc;n';
$lang['automatedtask_success']='Đ&atilde; thực hi&ecirc;n nhiệm vụ tự động';
$lang['backendwysiwygtouse']='Default backend wysiwyg (for new user accounts)';
$lang['cantchmodfiles']='Couldn&#039;t change permissions on some files';
$lang['content_autocreate_flaturls']='Automatically created URL&#039;s are flat';
$lang['content_autocreate_urls']='Automatically create page URL&#039;s';
$lang['content_imagefield_path']='Path for image field';
$lang['content_mandatory_urls']='Page URL&#039;s are required';
$lang['date_format_string_help']='<em>strftime</em> formatted date format string.  Try googling &#039;strftime&#039;';
$lang['default_contenttype']='Nội dung mặc định';
$lang['deleted_content']='Đ&atilde; xo&aacute; Nội dung';
$lang['deleted_group']='Đ&atilde; xo&aacute; Nh&oacute;m';
$lang['deletetemplate']='Delete Templates';
$lang['disallowed_contenttypes']='C&aacute;c kiểu nội dung như thế KH&Ocirc;NG được cho ph&eacute;p';
$lang['edited_content']='Nội dung đ&atilde; sửa';
$lang['edited_gcb']='Soạn khối nội dung to&agrave;n cục';
$lang['edited_udt']='Đ&atilde; sửa thẻ do người d&ugrave;ng định nghĩa';
$lang['edited_user']='Đ&atilde; sửa Người d&ugrave;ng';
$lang['enablesitedown']='Enable Site Down Message';
$lang['enablewysiwyg']='Enable WYSIWYG on Site Down Message';
$lang['error_contenttype']='Kiểu nội dung được g&aacute;n với trang n&agrave;y kh&ocirc;ng hợp lệ hoặc kh&ocirc;ng được cho ph&eacute;p';
$lang['error_no_default_content_block']='No default content block was detected in this template.  Please ensure that you have a {content} tag in the page template.';
$lang['event_desc_loginfailed']='Đăng nhập hỏng';
$lang['event_help_addglobalcontentpost']='<p>Sent after a new global content block is created.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;global_content&#039; - Reference to the affected global content block object.</li>
</ul>
';
$lang['event_help_addglobalcontentpre']='<p>Sent before a new global content block is created.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;global_content&#039; - Reference to the affected global content block object.</li>
</ul>
';
$lang['event_help_addgrouppost']='<p>Sent after a new group is created.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;group&#039; - Reference to the affected group object.</li>
</ul>
';
$lang['event_help_addgrouppre']='<p>Sent before a new group is created.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;group&#039; - Reference to the affected group object.</li>
</ul>
';
$lang['event_help_addstylesheetpost']='<p>Sent after a new stylesheet is created.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;stylesheet&#039; - Reference to the affected stylesheet object.</li>
</ul>
';
$lang['event_help_addstylesheetpre']='<p>Sent before a new stylesheet is created.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;stylesheet&#039; - Reference to the affected stylesheet object.</li>
</ul>
';
$lang['event_help_addtemplatepost']='<p>Sent after a new template is created.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;template&#039; - Reference to the affected template object.</li>
</ul>
';
$lang['event_help_addtemplatepre']='<p>Sent before a new template is created.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;template&#039; - Reference to the affected template object.</li>
</ul>
';
$lang['event_help_adduserpost']='<p>Sent after a new user is created.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;user&#039; - Reference to the affected user object.</li>
</ul>
';
$lang['event_help_adduserpre']='<p>Sent before a new user is created.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;user&#039; - Reference to the affected user object.</li>
</ul>
';
$lang['event_help_changegroupassignpost']='<p>Sent after group assignments are saved.</p>
<h4>Parameters></h4>
<ul>
<li>&#039;group&#039; - Reference to the affected group object.</li>
<li>&#039;users&#039; - Array of references to user objects now belonging to the affected group.</li>
';
$lang['event_help_changegroupassignpre']='<p>Sent before group assignments are saved.</p>
<h4>Parameters></h4>
<ul>
<li>&#039;group&#039; - Reference to the group object.</li>
<li>&#039;users&#039; - Array of references to user objects belonging to the group.</li>
';
$lang['event_help_contentdeletepost']='<p>Sent after content is deleted from the system.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;content&#039; - Reference to the affected content object.</li>
</ul>
';
$lang['event_help_contentdeletepre']='<p>Sent before content is deleted from the system.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;content&#039; - Reference to the affected content object.</li>
</ul>
';
$lang['event_help_contenteditpost']='<p>Sent after edits to content are saved.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;content&#039; - Reference to the affected content object.</li>
</ul>
';
$lang['event_help_contenteditpre']='<p>Sent before edits to content are saved.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;global_content&#039; - Reference to the affected content object.</li>
</ul>
';
$lang['event_help_contentpostcompile']='<p>Sent after content has been processed by smarty.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;content&#039; - Reference to the affected content text.</li>
</ul>
';
$lang['event_help_contentpostrender']='<p>Sent before the combined html is sent to the browser.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;content&#039; - Reference to the html text.</li>
</ul>
';
$lang['event_help_contentprecompile']='<p>Sent before content is sent to smarty for processing.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;content&#039; - Reference to the affected content text.</li>
</ul>
';
$lang['event_help_contentstylesheet']='<p>Sent before the sytlesheet is sent to the browser.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;content&#039; - Reference to the affected stylesheet text.</li>
</ul>
';
$lang['event_help_deleteglobalcontentpost']='<p>Sent after a global content block is deleted from the system.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;global_content&#039; - Reference to the affected global content block object.</li>
</ul>
';
$lang['event_help_deleteglobalcontentpre']='<p>Sent before a global content block is deleted from the system.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;global_content&#039; - Reference to the affected global content block object.</li>
</ul>
';
$lang['event_help_deletegrouppost']='<p>Sent after a group is deleted from the system.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;group&#039; - Reference to the affected group object.</li>
</ul>
';
$lang['event_help_deletegrouppre']='<p>Sent before a group is deleted from the system.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;group&#039; - Reference to the affected group object.</li>
</ul>
';
$lang['event_help_deletestylesheetpost']='<p>Sent after a stylesheet is deleted from the system.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;stylesheet&#039; - Reference to the affected stylesheet object.</li>
</ul>
';
$lang['event_help_deletestylesheetpre']='<p>Sent before a stylesheet is deleted from the system.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;stylesheet&#039; - Reference to the affected stylesheet object.</li>
</ul>
';
$lang['event_help_deletetemplatepost']='<p>Sent after a template is deleted from the system.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;template&#039; - Reference to the affected template object.</li>
</ul>
';
$lang['event_help_deletetemplatepre']='<p>Sent before a template is deleted from the system.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;template&#039; - Reference to the affected template object.</li>
</ul>
';
$lang['event_help_deleteuserpost']='<p>Sent after a user is deleted from the system.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;user&#039; - Reference to the affected user object.</li>
</ul>
';
$lang['event_help_deleteuserpre']='<p>Sent before a user is deleted from the system.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;user&#039; - Reference to the affected user object.</li>
</ul>
';
$lang['event_help_editglobalcontentpost']='<p>Sent after edits to a global content block are saved.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;global_content&#039; - Reference to the affected global content block object.</li>
</ul>
';
$lang['event_help_editglobalcontentpre']='<p>Sent before edits to a global content block are saved.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;global_content&#039; - Reference to the affected global content block object.</li>
</ul>
';
$lang['event_help_editgrouppost']='<p>Sent after edits to a group are saved.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;group&#039; - Reference to the affected group object.</li>
</ul>
';
$lang['event_help_editgrouppre']='<p>Sent before edits to a group are saved.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;group&#039; - Reference to the affected group object.</li>
</ul>
';
$lang['event_help_editstylesheetpost']='<p>Sent after edits to a stylesheet are saved.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;stylesheet&#039; - Reference to the affected stylesheet object.</li>
</ul>
';
$lang['event_help_editstylesheetpre']='<p>Sent before edits to a stylesheet are saved.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;stylesheet&#039; - Reference to the affected stylesheet object.</li>
</ul>
';
$lang['event_help_edittemplatepost']='<p>Sent after edits to a template are saved.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;template&#039; - Reference to the affected template object.</li>
</ul>
';
$lang['event_help_edittemplatepre']='<p>Sent before edits to a template are saved.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;template&#039; - Reference to the affected template object.</li>
</ul>
';
$lang['event_help_edituserpost']='<p>Sent after edits to a user are saved.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;user&#039; - Reference to the affected user object.</li>
</ul>
';
$lang['event_help_edituserpre']='<p>Sent before edits to a user are saved.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;user&#039; - Reference to the affected user object.</li>
</ul>
';
$lang['event_help_globalcontentpostcompile']='<p>Sent after a global content block has been processed by smarty.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;global_content&#039; - Reference to the affected global content block text.</li>
</ul>
';
$lang['event_help_globalcontentprecompile']='<p>Sent before a global content block is sent to smarty for processing.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;global_content&#039; - Reference to the affected global content block text.</li>
</ul>
';
$lang['event_help_loginpost']='<p>Sent after a user logs into the admin panel.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;user&#039; - Reference to the affected user object.</li>
</ul>
';
$lang['event_help_logoutpost']='<p>Sent after a user logs out of the admin panel.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;user&#039; - Reference to the affected user object.</li>
</ul>
';
$lang['event_help_smartypostcompile']='<p>Sent after any content destined for smarty has been processed.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;content&#039; - Reference to the affected text.</li>
</ul>
';
$lang['event_help_smartyprecompile']='<p>Sent before any content destined for smarty is sent to for processing.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;content&#039; - Reference to the affected text.</li>
</ul>
';
$lang['event_help_templatepostcompile']='<p>Sent after a template has been processed by smarty.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;template&#039; - Reference to the affected template text.</li>
</ul>
';
$lang['event_help_templateprecompile']='<p>Sent before a template is sent to smarty for processing.</p>
<h4>Parameters</h4>
<ul>
<li>&#039;template&#039; - Reference to the affected template text.</li>
</ul>
';
$lang['filecreatedirbadchars']='Ph&aacute;t hiện c&aacute;c k&yacute; tự kh&ocirc;ng hợp lệ trong t&ecirc;n thư mục';
$lang['filecreatedirnodoubledot']='Directory cannot contain &#039;..&#039;.';
$lang['filecreatedirnoslash']='Directory cannot contain &#039;/&#039; or &#039;\&#039;.';
$lang['filterapplied']='Bộ lọc hiện tại';
$lang['general_operation_settings']='C&aacute;c thiết lập vận h&agrave;nh chung';
$lang['helpaddtemplate']='<p>A template is what controls the look and feel of your site&#039;s content.</p><p>Create the layout here and also add your CSS in the Stylesheet section to control the look of your various elements.</p>';
$lang['hide_help_links']='Hide module help link';
$lang['info_edituser_password']='Change this field to change the user&#039;s password';
$lang['info_edituser_passwordagain']='Change this field to change the user&#039;s password';
$lang['info_sitedownexcludes']='This parameter allows listing a comma separated list of ip addresses or networks that should not be subject to the Site Down mechanism.  This allows administrators to work on a site whilst anonymous visitors receive a Site Down message.<br/><br/>Addresses can be specified in the following formats:<br/>
1. xxx.xxx.xxx.xxx -- (exact IP address)<br/>
2. xxx.xxx.xxx.[yyy-zzz] -- (IP address range)<br/>
3. xxx.xxx.xxx.xxx/nn -- (nnn = number of bits, cisco style.  i.e:  192.168.0.100/24 = entire 192.168.0 class C subnet)';
$lang['info_smarty_cacheudt']='Nếu được bật, mọi lời gọi tới thẻ người d&ugrave;ng định nghĩa sẽ được lưu dạng đệm.  Chế độ n&agrave;y sẽ hữu dụng cho c&aacute;c thẻ hiển thị kết quả c&aacute;c c&acirc;u truy vấn cơ sở dữ liệu.  Bạn c&oacute; thể tắt bộ đệm bằng c&aacute;ch d&ugrave;ng tham số nocache trong lời gọi udt.  v&iacute; dụ: <code>{myusertag nocache}</code>';
$lang['info_smarty_options']='C&aacute;c chọn lựa sau chỉ c&oacute; t&aacute;c dụng khi c&aacute;c tuỳ chọn bộ đệm b&ecirc;n tr&ecirc;n được bật';
$lang['installed_mod']='Phi&ecirc;n bản đ&atilde; c&agrave;i %s';
$lang['ip_addr']='Địa chỉ IP';
$lang['login_info_params']='<ol> 
  <li>Cookies must be enabled in your browser</li> 
  <li>Javascript must be enabled in your browser</li> 
  <li>Popup windows must be allowed for the following address:</li> 
</ol>';
$lang['lostpwemail']='You are recieving this e-mail because a request has been made to change the (%s) password associated with this user account (%s).  If you would like to reset the password for this account simply click on the link below or paste it into the url field on your favorite browser:
%s

If you feel this is incorrect or made in error, simply ignore the email and nothing will change.';
$lang['moduledecides']='Do thiết lập của module';
$lang['modulehelp_english']='Xem bằng tiếng Anh';
$lang['modulehelp_yourlang']='Xem với ng&ocirc;n ngữ của bạn';
$lang['moduleupgraded']='N&acirc;ng cấp th&agrave;nh c&ocirc;ng';
$lang['needpermissionto']='You need the &#039;%s&#039; permission to perform that function.';
$lang['never']='Kh&ocirc;ng bao giờ';
$lang['none']='none';
$lang['nopasswordforrecovery']='No email address set for this user.  Password recovery is not possible.  Please contact your administrator.';
$lang['nopluginabout']='Kh&ocirc;ng c&oacute; th&ocirc;ng tin giới thiệu về plugin n&agrave;y';
$lang['nopluginhelp']='Kh&ocirc;ng c&oacute; hướng dẫn cho plugin n&agrave;y';
$lang['prefsupdated']='User preferences has been updated.';
$lang['prompt_smarty_cachemodules']='Lưu bộ đệm c&aacute;c lời gọi module';
$lang['prompt_smarty_cacheudt']='Bộ đệm gọi UDT';
$lang['prompt_smarty_compilecheck']='Thực hiện kiểm tra bi&ecirc;n dịch';
$lang['prompt_use_smartycaching']='Bật bộ đệm Smarty';
$lang['runuserplugin']='Run User Plugin';
$lang['search_module']='Th&agrave;nh phần t&igrave;m kiếm';
$lang['showbookmarks']='Show Admin Shortcuts';
$lang['sitedown_settings']='Site Down Settings';
$lang['sitedownexcludes']='Exclude these Addresses from Site Down Messages';
$lang['sitedownwarning']='<strong>Warning:</strong> Your site is currently showing a &quot;Site Down for Maintenance&quot; message. Remove the %s file to resolve this.';
$lang['siteprefsupdated']='Đ&atilde; cập nhật c&aacute;c thiết lập to&agrave;n cục';
$lang['smarty_settings']='C&aacute;c thiết lập Smarty';
$lang['systemmaintenancedescription']='Various functions for maintaining the health of your system. You can also browser the changelog of CMSMadeSimple';
$lang['toggle']='Đổi';
$lang['uninstalled_mod']='Gỡ bỏ module %s';
$lang['upgraded_mod']='%s Đ&atilde; n&acirc;ng cấp từ Phi&ecirc;n bản %s l&ecirc;n %s';
$lang['warn_admin_ipandcookies']='Cảnh b&aacute;o: C&aacute;c hoạt động Quản trị sử dụng cookies v&agrave; ghi lại địa chỉ IP của bạn';
$lang['warning_safe_mode']='<strong><em>WARNING:</em></strong> PHP Safe mode is enabled.  This will cause difficulty with files uploaded via the web browser interface, including images, theme and module XML packages.  You are advised to contact your site administrator to see about disabling safe mode.';
$lang['xmlreader_class']='Đang kiểm tra lớp XMLReader';
?>